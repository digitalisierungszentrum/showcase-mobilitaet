import cv2
from ultralytics import YOLO
import math

# Load Yolo Model
model = YOLO("yolo-Weights/yolov8n.pt")

# Get Default Camera
cap = cv2.VideoCapture(0)
cap.set(3, 640)
cap.set(4, 480)

# Prepare a mapping for real classnames. The order represents indexes by yolo
class_names = ["Person", "Fahrrad", "Auto", "Motorrad", "Flugzeug", "Bus", "Zug", "LKW", "Boot",
              "Ampel", "Hydrant", "Stoppschild", "Parkuhr", "Sitzbank", "Vogel", "Katze",
              "Hund", "Pferd", "Schaf", "Kuh", "Elefant", "Baer", "Zebra", "Giraffe", "Rucksack", "Regenschirm",
              "Handtasche", "Krawatte", "Koffer", "Frisbee", "Ski", "Snowboard", "Sportball", "Drachen", "Baseballschlaeger",
              "Baseballhandschuh", "Skateboard", "Surfboard", "Tennisschlaeger", "Flasche", "Weinglass", "Tasse",
              "Gabel", "Messer", "Loeffel", "Schuessel", "Banane", "Apfel", "Sandwich", "Orange", "Brokkoli",
              "Karotte", "Hot Dog", "Pizza", "Donut", "Kuchen", "Stuhl", "Sofa", "Topfpflanze", "Bett",
              "Esstisch", "Toilette", "Fernseher", "Laptop", "Computermaus", "Fernbedienung", "Keyboard", "Handy",
              "Mikrowelle", "Ofen", "Toaster", "Spuele", "Kuehlschrank", "Buch", "Uhr", "Vase", "Schere",
              "Teddybaer", "Foehn", "Zahnbuerste"
              ]

while True:
    # Read image and apply model
    success, img = cap.read()
    results = model(img, stream=True)
    for r in results:
        boxes = r.boxes
        for box in boxes:
            # Get bounding box coordinates
            x1, y1, x2, y2 = box.xyxy[0]
            x1, y1, x2, y2 = int(x1), int(y1), int(x2), int(y2)
            color = (255, 0, 0)
            thickness = 2

            confidence = math.ceil((box.conf[0] * 100))
            # Include only results above threshold
            if confidence > 60:
                class_name = class_names[int(box.cls[0])]
                text_position = [x1, y1]
                font = cv2.FONT_HERSHEY_SIMPLEX
                fontScale = 1
                text_color = (255, 0, 0)
                text_thickness = 2

                label_text = f"{class_name} ({confidence}%)"
                cv2.putText(img, label_text, text_position, font, fontScale, text_color, text_thickness)

                cv2.rectangle(img, (x1, y1), (x2, y2), color, thickness)
    cv2.imshow('Objekterkennung', img)
    # Quit with key q
    if cv2.waitKey(1) == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
