# Showcase - Objekterkennung

Diese Anwendung dient dazu, Objekte in Echtzeit zu erkennen.

![Beispiel](example.png)

Dazu wird im Hintergrund das [YOLOv8](https://docs.ultralytics.com/) Modell verwendet.

## Installation und Start der Anwendung

Zuerst müssen die benötigten Pakete installiert werden.
Dies geschieht mittels des folgenden Befehl:

```shell
pip install -r requirements.txt
```

Um die Anwendung zu starten, muss das Python-Script über den folgenden Befehl gestartet werden:

```shell
python main.py
```

Beim ersten Start der Anwendung werden automatisch die benötigeten Gewichte des verwendeten Modells heruntergeladen.

Mit der Taste `q` kann die Anwendung beendet werden.

## Erkannte Objekte

Das verwendete Modell ist in der Lage folgende Klassen zu erkennen:

- Person
- Fahrrad
- Auto
- Motorrad
- Flugzeug
- Bus
- Zug
- LKW
- Boot
- Ampel
- Hydrant
- Stoppschild
- Parkuhr
- Sitzbank
- Vogel
- Katze
- Hund
- Pferd
- Schaf
- Kuh
- Elefant
- Bär
- Zebra
- Giraffe
- Rucksack
- Regenschirm
- Handtasche
- Krawatte
- Koffer
- Frisbee
- Ski
- Snowboard
- Sportball
- Drachen
- Baseballschläger
- Baseballhandschuh
- Skateboard
- Surfboard
- Tennisschläger
- Flasche
- Weinglass
- Tasse
- Gabel
- Messer
- Löffel
- Schüssel
- Banane
- Apfel
- Sandwich
- Orange
- Brokkoli
- Karotte
- Hot Dog
- Pizza
- Donut
- Kuchen
- Stuhl
- Sofa
- Topfpflanze
- Bett
- Esstisch
- Toilette
- Fernseher
- Laptop
- Computermaus
- Fernbedienung
- Keyboard
- Handy
- Mikrowelle
- Ofen
- Toaster
- Spüle
- Kühlschrank
- Buch
- Uhr
- Vase
- Schere
- Teddybär
- Föhn
- Zahnbürste
