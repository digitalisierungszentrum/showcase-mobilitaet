from flask import Flask, send_file
from flask_cors import CORS
import os


app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "http://localhost:1234"}})


@app.route("/image/<path:file_path>")
def serve_image(file_path):
    file_path = file_path.split("/")
    file_path = os.path.join(os.pardir, *file_path)
    return send_file(file_path, mimetype="image/jpeg")
