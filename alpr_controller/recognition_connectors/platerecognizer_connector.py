import requests
from result import Result


class PlateRecognizerConnector:
    def analyze_plate(self, device, image_content):
        # save image to disk
        # TODO: Extract this
        with open(f"assets/{device['camera_name']}/image.jpg", "wb") as f:
            f.write(image_content)

        regions = ["de-de"]

        # TODO: Extract api and token to config
        response = requests.post(
            "https://api.platerecognizer.com/v1/plate-reader/",
            data=dict(regions=regions),
            files=dict(upload=image_content),
            headers={"Authorization": "Token xxx"},
        )

        if response.status_code >= 200 and response.status_code < 300:
            with open("response.json", "wb") as f:
                f.write(response.content)

            result = Result.load_json(response.text)
            print(result)
            return result.results
        else:
            print("Failed to send the image. Status code:", response.status_code)
            return None
