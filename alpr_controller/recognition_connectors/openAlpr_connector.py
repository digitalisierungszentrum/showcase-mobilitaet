import json
import subprocess
import os

from result import Result


class OpenAlprConnector:
    def analyze_plate(self, image_path):
        try:
            result = subprocess.run(
                f"docker run -it --rm -v {os.getcwd()}:/data:ro openalpr -d -j -c eu {image_path}",
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                text=True,
            )

            if result.returncode == 0:
                return self.__format_response(result.stdout)
            else:
                return result.stderr
        except Exception as e:
            return str(e)

    def __format_response(self, raw_json):
        resultJson = json.loads(raw_json)["results"]
        data = []
        for result in resultJson:
            box = self.__format_coordinates(result["coordinates"])
            candidates = self.__format_candidates(result["candidates"])
            data.append(
                {
                    "plate": result["plate"],
                    "score": result["confidence"],
                    "box": box,
                    "candidates": candidates,
                    "vehicle": "",  # currently empty, might be added by yolo
                }
            )
        return Result.load_list(data)

    def __format_candidates(self, candidates):
        return [
            {
                "score": candidate["confidence"],
                "plate": candidate["plate"],
            }
            for candidate in candidates
        ]

    def __format_coordinates(self, coordinates):
        x_values = [coord["x"] for coord in coordinates]
        y_values = [coord["y"] for coord in coordinates]

        xmin = min(x_values)
        ymin = min(y_values)
        xmax = max(x_values)
        ymax = max(y_values)

        return {"xmin": xmin, "ymin": ymin, "xmax": xmax, "ymax": ymax}

    # percentage = fuzz.ratio("SVK2004", plate)
    # print(f"plate: {plate}, confidence {confidence}, percentage {percentage}")


# {
#     "version": 2,
#     "data_type": "alpr_results",
#     "epoch_time": 1692871655689,
#     "img_width": 1600,
#     "img_height": 1200,
#     "processing_time_ms": 239.23439,
#     "regions_of_interest": [{"x": 0, "y": 0, "width": 1600, "height": 1200}],
#     "results": [
#         {
#             "plate": "SVK20O4",
#             "confidence": 87.602989,
#             "matches_template": 0,
#             "plate_index": 0,
#             "region": "",
#             "region_confidence": 0,
#             "processing_time_ms": 27.852276,
#             "requested_topn": 1,
#             "coordinates": [
#                 {"x": 759, "y": 952},
#                 {"x": 846, "y": 957},
#                 {"x": 846, "y": 978},
#                 {"x": 759, "y": 973},
#             ],
#             "candidates": [
#                 {"plate": "SVK20O4", "confidence": 87.602989, "matches_template": 0}
#             ],
#         }
#     ],
# }
