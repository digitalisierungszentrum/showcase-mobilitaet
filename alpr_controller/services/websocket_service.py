import threading
from websocket_server import WebsocketServer


class WebSocketConnection:
    def __init__(self, host: str, port: int) -> None:
        self._port = port
        self._host = host
        self.server = None

    def start(self):
        self.server = WebsocketServer(host=self._host, port=self._port)
        wst = threading.Thread(target=lambda: self.server.run_forever())
        wst.daemon = True
        wst.start()

        return self

    def send_action(self, message: str):
        self.server.send_message_to_all(message)
