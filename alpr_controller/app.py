import json
import requests
import subprocess
from fuzzywuzzy import fuzz
import cv2
import time
import numpy as np
from recognition_connectors.openAlpr_connector import OpenAlprConnector
from recognition_connectors.platerecognizer_connector import (
    PlateRecognizerConnector,
)
from services.websocket_service import WebSocketConnection


from result import Result

with open("config.json") as config_file:
    config = json.load(config_file)

TESTING = config.get("testing")
devices = config.get("devices")


def get_current_image(device):
    # for testing
    if TESTING:
        with open(f"assets/{device['camera_name']}/image.jpg", "rb") as f:
            return f.read()

    url = "http://" + device["ip"] + "/snapshot"
    response = requests.get(url)

    if response.status_code == 200:
        image_content = response.content
        return image_content
    else:
        print("Failed to download the image. Status code:", response.status_code)
        return None


def get_license_plates(device, image_content):
    # for testing
    if TESTING:  # TODO: Change this back to if testing
        with open(f"assets/{device['camera_name']}/response.json", "rb") as f:
            result = Result.load_json(f.read())
            return result.results
    elif config.get("connector") == "openalpr":
        connector = OpenAlprConnector()
    elif config.get("connector") == "platerecognizer":
        pass
        connector = PlateRecognizerConnector()
    else:
        return None
    # TODO: get path from input to function
    image_path = f"assets/{device['camera_name']}/image.jpg"
    return connector.analyze_plate(image_path)


def match_license_plates(device, found_lps):
    # for each expected license plate
    for expected_lp in device["expectedLPs"]:
        # for each found license plate
        for found_lp in found_lps:
            # if found license plate matches expected license plate
            print(expected_lp["search"])
            percentage = fuzz.ratio(expected_lp["search"], found_lp.plate)
            if percentage > config.get("fuzzy_search_threshold"):
                # send action to device
                print("Found license plate: " + found_lp.plate)
                return {
                    "plate": expected_lp["label"],
                    "box": found_lp.box,
                    "vehicle": found_lp.vehicle,
                    "score": found_lp.score,
                    "action": device["action"],
                }

    found_lp = found_lps[0]
    return {
        "plate": found_lp.plate,
        "box": found_lp.box,
        "vehicle": found_lp.vehicle,
        "score": found_lp.score,
        "action": None,
    }


def scale_coord(coordinate, scale=0.7):
    return int(coordinate * scale)


def send_image(device, image_content, matching_info):
    image_array = np.frombuffer(image_content, np.uint8)
    loaded_image = cv2.imdecode(image_array, cv2.IMREAD_COLOR)

    new_width = scale_coord(loaded_image.shape[1])
    new_height = scale_coord(loaded_image.shape[0])

    img = cv2.resize(loaded_image, (new_width, new_height))
    box = matching_info["box"]

    if matching_info:
        padding = 5
        cv2.rectangle(
            img,
            (scale_coord(box["xmin"]) - padding, scale_coord(box["ymin"]) - padding),
            (scale_coord(box["xmax"]) + padding, scale_coord(box["ymax"]) + padding),
            (0, 255, 0) if matching_info["action"] else (0, 0, 255),
            3,
        )

        if matching_info["vehicle"]:
            vbox = matching_info["vehicle"]["box"]
            img = img[
                scale_coord(vbox["ymin"]) : scale_coord(vbox["ymax"]),
                scale_coord(vbox["xmin"]) : scale_coord(vbox["xmax"]),
            ]

    cv2.imwrite(f"assets/{device['camera_name']}/image_rect.jpg", img)


def create_device_folder(device):
    import os

    current_directory = os.getcwd()
    new_folder_path = os.path.join(current_directory, f"assets/{device['camera_name']}")
    if not os.path.exists(new_folder_path):
        os.makedirs(new_folder_path)


def main():
    websocket = open_websocket_connection()
    start_flask_server()

    try:
        while True:
            time.sleep(5)
            # for each device
            for device in devices:
                create_device_folder(device)
                image_content = get_current_image(device)
                result = get_license_plates(device, image_content)

                if result is None:
                    send_image(device, image_content, None)
                    continue

                found_lps = result
                if len(found_lps) == 0:
                    send_image(device, image_content, None)
                    continue

                matching_license_plate_info = match_license_plates(device, found_lps)
                send_image(device, image_content, matching_license_plate_info)
                data = {
                    "device": device["camera_name"],
                    "plate": matching_license_plate_info["plate"],
                    "score": matching_license_plate_info["score"] * 100,
                    "action": matching_license_plate_info["action"],
                }
                websocket.send_action(json.dumps(data))

    except KeyboardInterrupt:
        print("interrupted!")

        # invoke action if recognized
        # TODO what is an action?


def open_websocket_connection():
    websocket = WebSocketConnection("127.0.0.1", 8865)
    websocket.start()
    return websocket


def start_flask_server():
    try:
        result = subprocess.run(
            "flask --app ./api/flask_server.py run --port=5005",
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
        )
        if result.returncode == 0:
            return result.stdout
        else:
            return result.stderr
    except Exception as e:
        return str(e)


if __name__ == "__main__":
    main()
