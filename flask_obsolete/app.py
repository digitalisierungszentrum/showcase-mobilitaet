from flask import Flask, request, jsonify, render_template, Response, redirect, url_for, send_from_directory
from flask_bootstrap import Bootstrap
from flask_sse import sse
from fuzzywuzzy import fuzz
import time
import cv2

app = Flask(__name__)
Bootstrap(app)

license_plates = {"ULDZ112": "UL-DZ 112", "SVK2004":"S-VK 2004", "WGGS762": "WG-GGS 762"}

lpdata = ""

def match_lp(jsoncontent):
    for lp in license_plates.keys():
        capturedlp = jsoncontent['results'][0]['plate']
        #print(capturedlp, " : ", fuzz.ratio(lp, capturedlp))
        percentage = fuzz.ratio(lp, capturedlp)
        global lpdata
        print(percentage, capturedlp)
        if (percentage > 85):
            lpimg = cv2.imread('images/%s.jpg' % jsoncontent['uuid'])
            coordinates = jsoncontent['results'][0]['coordinates']
            padding = 10
            cv2.rectangle(lpimg, (coordinates[0]['x']-padding, coordinates[0]['y']-padding), (coordinates[2]['x']+padding, coordinates[2]['y']+padding), (0, 255, 0), 5)
            lptext = str(percentage) + "%: " + license_plates[lp]
            cv2.putText(lpimg, lptext, (coordinates[0]['x']-padding, coordinates[0]['y']-30), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (0, 255, 0), 4, cv2.LINE_AA)
            cv2.imwrite('images/%s.jpg' % jsoncontent['uuid'], lpimg)
            lpdata = "<img src='images/%s.jpg' alt='%s = %s:%s' width='600'>" % (jsoncontent['uuid'], capturedlp, percentage,lp)





@app.route('/images/<path:filepath>')
def data(filepath):
    return send_from_directory('images', filepath)

@app.route('/push/', methods=['GET', 'POST'])
def add_message():
    content = request.json
    match_lp(content)
    return "OK"

def get_message():
    '''this could be any function that blocks until data is ready'''
    time.sleep(1.0)
    s = time.ctime(time.time())
    return s

@app.route('/')
def root():
    return render_template('index.html')

@app.route('/stream')
def stream():
    def eventStream(lpdata):
        if lpdata:
            yield 'data: {}\n\n'.format(lpdata)
            lpdata=''
            time.sleep(.1)
    return Response(eventStream(lpdata), mimetype="text/event-stream")

if __name__ == '__main__':
    app.run(host= '0.0.0.0',port='8090',debug=True)

#{'version': 2, 'data_type': 'alpr_results', 'epoch_time': 1678112580304, 'img_width': 1600, 'img_height': 1200, 'processing_time_ms': 855.577942, 'regions_of_interest': [], 'results': [{'plate': 'ULDZ12', 'confidence': 78.609375, 'matches_template': 0, 'plate_index': 0, 'region': '', 'region_confidence': 0, 'processing_time_ms': 46.273426, 'requested_topn': 1, 'coordinates': [{'x': 634, 'y': 750}, {'x': 729, 'y': 746}, {'x': 729, 'y': 765}, {'x': 634, 'y': 769}], 'candidates': [{'plate': 'ULDZ12', 'confidence': 78.609375, 'matches_template': 0}]}], 'uuid': 'Demo-cam1-1678112581175', 'camera_id': 1, 'site_id': 'Demo', 'company_id': 'DZ'}
