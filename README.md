### Links & Co

- ESP32-Cam Firmware: https://github.com/rzeldent/esp32cam-rtsp

- Change touch orientation: https://stackoverflow.com/questions/71208290/portrait-screen-touch-vertical-rotation-on-ubuntu-20-04-doesnt-work

# run alpr_ui in development

`npx parcel ./src/index.html`

# run alpr_controller in development

`python app.py`

# [deprecated, is auto started] run flask endpoint in development

`flask --app flask_server.py run --port=5005`
