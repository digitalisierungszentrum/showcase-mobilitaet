import { getDefaultStore } from "jotai";
import { messageAtom } from "../../services/serviceAtoms";
import { imageFetchingSevice } from "../../services/imageFetchingService";
import { useEffect, useState } from "react";
import { useDisclosure } from "@chakra-ui/react";
import ImageModal from "../Modal/ImageModal";
import ImageCard from "../Card/ImageCard";

interface Data {
  device: string;
  plate: string;
  score: number;
  action: string;
}

const ImageDisplay = ({ position }) => {
  const [data, setData] = useState<Data>({
    device: "",
    plate: "",
    score: 0,
    action: "",
  });
  const [imageSrc, setImageSrc] = useState(null);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [displayNew, setDisplayNew] = useState(true);

  const fetchImage = async () => {
    await imageFetchingSevice(position).then((res) => {
      setImageSrc(res);
    });
  };

  const store = getDefaultStore();
  useEffect(() => {
    const unsub = store.sub(messageAtom, () => {
      const newMessage = store.get(messageAtom);
      var parsedData = JSON.parse(newMessage);

      if (parsedData.device === position) {
        setData(parsedData);
        fetchImage();
        setDisplayNew(true);
        setTimeout(() => {
          setDisplayNew(false);
        }, 2000);
      }
    });

    return () => unsub();
  }, []);
  const tableData = [
    {
      label: "Erkanntes Kennzeichen:",
      value: data.plate,
    },
    {
      label: "Erkennungs Score:",
      value: `${data.score}%`,
    },
    {
      label: "Action Triggered:",
      value: data.action,
    },
  ];
  return (
    <>
      <ImageCard
        imageSrc={imageSrc}
        onOpen={onOpen}
        displayNew={displayNew}
        plate={data.plate}
      />
      <ImageModal
        headerLabel={"Fahrzeug"}
        isOpen={isOpen}
        onClose={onClose}
        imageSrc={imageSrc}
        tableData={tableData}
      />
    </>
  );
};

export default ImageDisplay;
