import {
  Card,
  CardBody,
  Heading,
  Image,
  Spinner,
  Badge,
} from "@chakra-ui/react";

const ImageCard = ({ imageSrc, onOpen, displayNew, plate }) => {
  return (
    <Card align={"center"} maxW="250px" padding={"10px"}>
      <CardBody>
        {imageSrc && (
          <Image
            cursor={"pointer"}
            onClick={onOpen}
            objectFit={"cover"}
            src={imageSrc}
            alt="LPT Image"
            borderRadius="lg"
          />
        )}
        {!imageSrc && <Spinner size="xl" thickness="4px" />}
      </CardBody>
      {imageSrc && displayNew && <Badge colorScheme="green">New</Badge>}
      <Heading size="md">{plate}</Heading>
    </Card>
  );
};

export default ImageCard;
