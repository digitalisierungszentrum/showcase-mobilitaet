import { Table, Tbody, Tr, Td, TableContainer } from "@chakra-ui/react";

const ModalTable = ({ tableData }) => {
  return (
    <TableContainer>
      <Table variant="simple" size={"lg"} fontWeight={"bold"}>
        <Tbody>
          {tableData.map((row, index) => (
            <Tr key={index}>
              <Td>{row.label}</Td>
              <Td>{row.value}</Td>
            </Tr>
          ))}
        </Tbody>
      </Table>
    </TableContainer>
  );
};

export default ModalTable;
