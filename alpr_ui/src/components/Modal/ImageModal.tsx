import {
  Image,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Container,
  VStack,
} from "@chakra-ui/react";
import ModalTable from "./ModalTable";

const ImageModal = ({ headerLabel, isOpen, onClose, imageSrc, tableData }) => {
  return (
    <Modal size={"xl"} isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent alignItems={"center"}>
        <ModalHeader>{headerLabel}</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Container height={"600px"}>
            <VStack>
              <Image
                objectFit={"cover"}
                src={imageSrc}
                alt="LPT Image"
                borderRadius="lg"
              />
              <ModalTable tableData={tableData} />
            </VStack>
          </Container>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

export default ImageModal;
