import config from "../config";

export async function imageFetchingSevice(position: string) {
  try {
    console.log(`${config.baseUrl}/image/assets/${position}/image_rect.jpg`);
    const response = await fetch(
      `${config.baseUrl}/image/assets/${position}/image_rect.jpg`
    );
    const blob = await response.blob();
    const imageUrl = URL.createObjectURL(blob);
    return imageUrl;
  } catch (error) {
    console.error(error);
  }
}
