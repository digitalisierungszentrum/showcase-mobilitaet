import { getDefaultStore } from "jotai";
import { messageAtom } from "./serviceAtoms";

class WebSocketService {
  private socket: WebSocket | null = null;
  private store: ReturnType<typeof getDefaultStore>;

  constructor(private url: string) {
    this.store = getDefaultStore();
    this.connect();
  }

  private connect() {
    this.socket = new WebSocket(this.url);

    this.socket.addEventListener("open", (event) => {
      console.log("WebSocket connection opened", event);
    });

    this.socket.addEventListener("message", (event) => {
      const message = event.data;
      this.store.set(messageAtom, message);
      console.log("WebSocket message received:", message);
    });

    this.socket.addEventListener("close", (event) => {
      console.log("WebSocket connection closed", event);
      this.reconnect();
    });

    this.socket.addEventListener("error", (event) => {
      console.error("WebSocket error occurred", event);
    });
  }

  private reconnect() {
    setTimeout(() => {
      console.log("WebSocket reconnecting...");
      this.connect();
    }, 1000);
  }

  send(message: string) {
    if (this.socket && this.socket.readyState === WebSocket.OPEN) {
      this.socket.send(message);
    } else {
      console.warn("WebSocket is not open. Cannot send message.");
    }
  }

  close() {
    if (this.socket) {
      this.socket.close();
    }
  }
}

export default WebSocketService;
