import { useEffect } from "react";
import "./App.scss";
import ImageDisplay from "./components/ImageDisplay/ImageDisplay";
import WebSocketService from "./services/webSocketService";
import { Provider, createStore } from "jotai";
import { Box, ChakraProvider } from "@chakra-ui/react";

function App() {
  const store = createStore();
  useEffect(() => {
    const wsUrl = "ws://localhost:8865";
    const webSocketService = new WebSocketService(wsUrl);

    return () => {
      webSocketService.close();
    };
  }, []);

  return (
    <ChakraProvider>
      <Provider store={store}>
        <div className="petrol-position">
          <ImageDisplay position="petrol_station" />
        </div>
        <div className="hospital-position">
          <ImageDisplay position="hospital" />
        </div>
      </Provider>
    </ChakraProvider>
  );
}

export default App;
